#include <cstdio>
#include <math.h>
#include <iostream>

const int screenHeight = 600;
const int screenWidth = 600;
const float epsilon = 10e-6;
const int NUMBER_OF_OBJECTS = 10;
const int MAX_NUMBER_OF_RECURSION = 10;

class Color {
public:
    float r, g, b;

    Color( ) {
        r = g = b = 0;
    }
    Color(float r0, float g0, float b0) {
        r = r0; g = g0; b = b0;
    }
    Color operator*(float a) {
        return Color(r * a, g * a, b * a);
    }
    Color operator-(float a) {
        return Color(r - a, g - a, b - a);
    }
    Color operator-(const Color& a) {
        return Color(r - a.r, g - a.g, b - a.b);
    }
    Color operator/(const Color& c) {
        return Color(r / c.r, g / c.g, b / c.b);
    }
    Color operator/(float a) {
        return Color(r / a, g / a, b / a);
    }
    Color operator*(const Color& c) {
        return Color(r * c.r, g * c.g, b * c.b);
    }
    Color operator+(const Color& c) {
        return Color(r + c.r, g + c.g, b + c.b);
    }
};

class Vector {
public:
    float x, y, z, w;

    Vector(){
        x = y = z = w = 0;
    }
    Vector(float x0, float y0, float z0, float w0 = 0) {
        x = x0; y = y0; z = z0; w = w0;
    }
    Vector operator*(float a) {
        return Vector(x * a, y * a, z * a);
    }
    Vector operator+(const Vector& v) {
        return Vector(x + v.x, y + v.y, z + v.z, w + v.w);
    }
    Vector operator-(const Vector& v) {
        return Vector(x - v.x, y - v.y, z - v.z, w - v.w);
    }
    float operator*(const Vector& v) {
        return (x * v.x + y * v.y + z * v.z);
    }
    Vector operator%(const Vector& v) {
        return Vector(y*v.z-z*v.y, z*v.x - x*v.z, x*v.y-y*v.x);
    }
    Vector operator/(float f) {
        return Vector(x/f, y/f, z/f);
    }
    bool operator==(const Vector& v){
        return (v.x==x && v.y==y && v.z==z);
    }
    bool operator!=(const Vector& v){
        return (v.x!=x || v.y!=y || v.z!=z);
    }
    float Length() { return (float)sqrt(x * x + y * y + z * z); }
};

Vector Normalize(Vector v) { return v * (1 / v.Length()); }

class Light {
public:
    Color color;
    Vector pos;
    Light(Color& c, Vector& p) : color(c), pos(p) {}
};

class Ray {
public:
    Vector origin, dir;
    float t;
    Ray(Vector& o, Vector& d) : origin(o), dir(d), t(-1) {}
};

class Camera {
public:
    Vector eye, lookat, up, right;
    Camera(Vector& e, Vector& l, Vector& u, Vector& r) : eye(e), lookat(l), up(u), right(r) {}
    Ray getRay(float x, float y) {
        Vector p = lookat + right * (2.0f * x / (float)screenWidth -1.0f) + up * (2.0f * y / (float)screenHeight -1.0f);
        Vector dir = Normalize(p - eye);
        return Ray(eye, dir);
    }
};

class Material {
public:
    Color color;
    Color F0, n, kn, ks;
    float shine;
    bool isReflective;
    Material(Color& c) : color(c), isReflective(false) {}
    Material(Color& n, Color& kn, Color& ks, float sh, bool ir) : n(n), kn(kn), ks(ks), shine(sh), isReflective(ir) {
        F0 = ((n - 1.0f) * (n - 1.0f) + ks * ks) / ((n - (-1.0f)) * (n - (-1.0f)) + ks * ks);
    }
    Color computeFresnel(float costheta) {
        Color u(1.0f, 1.0f, 1.0f);
        Color F = F0 + (u - F0) * pow(1 - costheta, 5);
        return F;
    }
};

class Hit {
public:
    Material material;
    Vector normal, position;
    float t;
    Hit(Color& ambient, float t) : material(ambient), normal(), position(), t(t) {}
};

class Object {
public:
    Material material;
    Object(Material& m) : material(m) {}
    virtual bool intersect(Ray& ray, Hit* hit) = 0;
};

class Scene {
public:
    Light light;
    Camera camera;
    Object* objects[NUMBER_OF_OBJECTS];
    int numObjects;
    Scene(Light& l, Camera& c) : light(l), camera(c), numObjects(0){}
    void addObject(Object* o) {
        if(numObjects < NUMBER_OF_OBJECTS) {
            objects[numObjects++] = o;
        }
    }
    Color trace(Ray ray, int rec) {
        Color retColor(0.0f, 0.0f, 0.0f);
        Hit hit(retColor, 10e8);
        Hit bestHit(retColor, 10e4);
        bool was_hit = false;
        if(rec < MAX_NUMBER_OF_RECURSION) {
            ++rec;
            for(int i=0; i<numObjects; i++) {
                if((objects[i]->intersect(ray, &hit))) {
                    if (hit.t < bestHit.t) {
                        bestHit.normal = hit.normal;
                        bestHit.position = hit.position;
                        bestHit.material = hit.material;
                        bestHit.t = hit.t;
                        was_hit = true;
                    }
                }
            }
            if(was_hit) {
                Vector sh_dir = Normalize(light.pos - bestHit.position);
                Vector sh_origin = bestHit.position + bestHit.normal * 0.0001f;
                Ray sh(sh_origin, sh_dir);
                float costheta = sh_dir * bestHit.normal;
                if (costheta < 0)
                    costheta = 0;
                if(!bestHit.material.isReflective) {
                    retColor = bestHit.material.color * light.color * costheta;
                } else {
                    Color F = bestHit.material.computeFresnel(costheta);
                    costheta = -1.0f * (ray.dir * bestHit.normal);
                    Vector refl_dir = Normalize(ray.dir + bestHit.normal * 2.0f * costheta);
                    Vector refl_origin = bestHit.position + bestHit.normal * 0.0001;
                    Ray refl(refl_origin, refl_dir);
                    Color refl_color = trace(refl, rec);
                    retColor = retColor + refl_color;
                    retColor = retColor * F;
                }
            }
        }
        return retColor;
    }
};

class Sphere : public Object {
public:
    Vector center;
    float rad;
    Sphere(Material& m, Vector& c, float r) : Object(m), center(c), rad(r) {}
    Vector getNormal(Vector& hit) {
        return Normalize(hit - center);
    }
    bool intersect(Ray& ray, Hit* hit) {
        float t = 0;
        float a = ray.dir * ray.dir;
        float b = (ray.origin - center) * 2.0f * ray.dir ;
        float c = (ray.origin - center) * (ray.origin - center) - rad * rad;
        float discr = b * b - 4.0f * a * c;
        float den = 2 * a;
        if (discr < epsilon)
            return false;
        t =  ((- b - (float)sqrt(discr)) / den);

        if(t > epsilon) {
            hit->t = t;
            hit->material = material;
            hit->position = ray.origin + ray.dir * t;
            hit->normal = getNormal(hit->position);
            return true;
        }
        t = ((- b + (float)sqrt(discr)) / den);
        //std::cout<<t<<"\n";
        if(t > epsilon) {
            hit->t = t;
            hit->material = material;
            hit->position = ray.origin + ray.dir * t;
            hit->normal = getNormal(hit->position);
            return true;
        }
        return false;
    }
};

class Plane : public Object {
public:
    Vector normal, point;
    Plane(Vector& n, Vector& p, Material& m) : normal(n), point(p), Object(m) {}
    bool intersect(Ray& ray, Hit* hit) {
        float t = (point - ray.origin)*normal / (ray.dir * normal);
        if(t>epsilon) {
            hit->normal = normal;
            hit->position = ray.origin + ray.dir * t;
            hit->material = material;
            hit->t = t;
            return true;
        }
        return false;
    }
};

int main() {
    Color white(1.0f, 1.0f, 1.0f);
    Color orange(0.89f, 0.6352f, 0.102f);
    Color blue(0.7f, 0.89f, 1.0f);
    Color grey(0.7f, 0.65f, 0.45f);

    Vector light_position(0.0f, 80.0f, 0.0f);
    Light light(white, light_position);

    Vector eye(0.0f, 0.0f, -200.0f);
    Vector lookat(0.0f, 0.0f, 0.0f);
    Vector up(0.0f, -(screenHeight / 2.0f), 0.0f);
    Vector right(screenWidth / 2.0f, 0.0f, 0.0f);
    Camera camera(eye, lookat, up, right);

    Material raw_orange(orange);
    Color gold_n(0.17f, 0.35f, 1.5f);
    Color ks(3.1f, 2.7f, 1.9f);
    Material gold(gold_n, ks, ks, 2.0f, true);
    Vector sphere1_center(20.0f, 0.0f, 100.0f);
    Sphere sphere1(raw_orange, sphere1_center, 70.0f);

    Material raw_blue(blue);
    Vector sphere2_center(100.0f, -10.0f, 200.0f);
    Sphere sphere2(raw_blue, sphere2_center, 100.0f);

    Material raw_grey(grey);
    Vector plane_point(0.0f, -100.0f, 0.0f);
    Vector plane_normal(0.0f, 1.0f, 0.1f);
    Plane plane(plane_normal, plane_point, raw_grey);

    Scene scene(light, camera);

    scene.addObject(&sphere2);
    scene.addObject(&sphere1);
    scene.addObject(&plane);

    Color **co = new Color*[screenWidth];
    for(int i=0; i<screenWidth; i++) {
        co[i] = new Color[screenHeight];
    }
    for(int j=0; j<screenHeight; j++) {
    for(int i=0; i<screenWidth; i++) {

            co[i][j] = scene.trace(camera.getRay((float)i,(float)j), 0);
        }
    }

    FILE* f;
    f = fopen("myray.ppm", "w");
    fprintf(f, "P3\n%d %d \n255\n ", screenWidth, screenHeight);
    for(int j=0; j<screenHeight; j++) {
        for(int i=0; i<screenWidth; i++) {

            fprintf(f, "%d ", (unsigned int)(co[i][j].r * 255));
            fprintf(f, "%d ", (unsigned int)(co[i][j].g * 255));
            fprintf(f, "%d ", (unsigned int)(co[i][j].b * 255));
        }
    }
    fclose(f);

    return 0;
}
